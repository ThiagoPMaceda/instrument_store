defmodule InstrumentStore.DataCase do
  @moduledoc """
  This module defines the setup for tests requiring
  access to the application's data layer.

  You may define functions here to be used as helpers in
  your tests.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use InstrumentStore.DataCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  import Hammox
  alias Ecto.Adapters.SQL.Sandbox
  alias InstrumentStore.Repo

  using do
    quote do
      alias InstrumentStore.Repo

      import Ecto

      import Hammox
      import Ecto.Changeset
      import Ecto.Query
      import InstrumentStore.DataCase
    end
  end

  setup tags do
    pid = Sandbox.start_owner!(Repo, shared: not tags[:async])
    on_exit(fn -> Sandbox.stop_owner(pid) end)
    :ok
  end

  setup :verify_on_exit!

  setup _ do
    Hammox.stub_with(WebServiceBehaviourMock, InstrumentStore.WebServiceStub)
    :ok
  end

  setup _ do
    on_exit(fn ->
      :timer.sleep(20)

      for pid <- Task.Supervisor.children(InstrumentStore.Worker) do
        ref = Process.monitor(pid)
        assert_receive {:DOWN, ^ref, _, _, _}, 1000
      end
    end)
  end

  @doc """
  A helper that transforms changeset errors into a map of messages.

      assert {:error, changeset} = Accounts.create_user(%{password: "short"})
      assert "password is too short" in errors_on(changeset).password
      assert %{password: ["password is too short"]} = errors_on(changeset)

  """
  def errors_on(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Regex.replace(~r"%{(\w+)}", message, fn _, key ->
        opts |> Keyword.get(String.to_existing_atom(key), key) |> to_string()
      end)
    end)
  end
end
