defmodule InstrumentStore.InstrumentsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `InstrumentStore.Instruments` context.
  """

  @doc """
  Generate a guitar.
  """
  alias InstrumentStore.Instruments.Guitar
  alias InstrumentStore.Repo

  def guitar_fixture do
    attrs = %{
      is_available: true,
      model: "some model",
      year: 42
    }

    {:ok, guitar} =
      %Guitar{}
      |> Guitar.changeset(attrs)
      |> Repo.insert()

    guitar
  end
end
