defmodule InstrumentStoreWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use InstrumentStoreWeb.ConnCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  alias Ecto.Adapters.SQL.Sandbox

  import Hammox

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import InstrumentStoreWeb.ConnCase
      import Hammox

      alias InstrumentStoreWeb.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint InstrumentStoreWeb.Endpoint
    end
  end

  setup _ do
    Hammox.stub_with(WebServiceBehaviourMock, InstrumentStore.WebServiceStub)
    :ok
  end

  setup _ do
    on_exit(fn ->
      :timer.sleep(20)

      for pid <- Task.Supervisor.children(InstrumentStore.Worker) do
        ref = Process.monitor(pid)
        assert_receive {:DOWN, ^ref, _, _, _}, 1000
      end
    end)
  end

  setup :verify_on_exit!

  setup tags do
    pid = Sandbox.start_owner!(InstrumentStore.Repo, shared: not tags[:async])
    on_exit(fn -> Sandbox.stop_owner(pid) end)
    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end
