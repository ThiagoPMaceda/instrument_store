defmodule InstrumentStoreWeb.FallbackTest do
  use InstrumentStoreWeb.ConnCase

  alias Ecto.Changeset
  alias InstrumentStoreWeb.Fallback
  alias StrongParams.Error

  describe "call/2 [ handle unprocessable entity]" do
    test "handle error with ecto changeset", %{conn: conn} do
      changeset = Changeset.add_error(%Changeset{types: %{}}, :key, "invalid format")

      response =
        conn
        |> Fallback.call({:error, changeset})
        |> json_response(422)

      assert response == %{
               "errors" => %{"key" => ["invalid format"]},
               "message" => "Unprocessable entity"
             }
    end
  end

  describe "call/2 [ handle bad request ]" do
    test "handle bad request error", %{conn: conn} do
      response =
        conn
        |> Fallback.call({:error, :bad_request})
        |> json_response(400)

      assert response == "Bad Request"
    end
  end

  describe "call/2 [ handle bad request :: params missing]" do
    test "handle bad request error when params are missing", %{conn: conn} do
      error = %Error{
        type: "required",
        errors: %{missingKeyOne: "is required", missingKeyTwo: "is required"}
      }

      response =
        conn
        |> Fallback.call(error)
        |> json_response(400)

      assert response == %{
               "message" => "Bad request",
               "errors" => %{"missingKeyOne" => "is required", "missingKeyTwo" => "is required"}
             }
    end
  end
end
