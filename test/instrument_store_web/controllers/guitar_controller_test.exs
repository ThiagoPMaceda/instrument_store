defmodule InstrumentStoreWeb.GuitarControllerTest do
  use InstrumentStoreWeb.ConnCase, async: true

  @create_attrs %{is_available: true, model: "Jackson", year: 42}
  @invalid_attrs %{is_available: nil, model: nil, year: nil}

  describe "index" do
    test "lists all guitars", %{conn: conn} do
      conn = get(conn, Routes.guitar_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Guitars"
    end
  end

  describe "new guitar" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.guitar_path(conn, :new))
      assert html_response(conn, 200) =~ "New Guitar"
    end
  end

  describe "create guitar" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.guitar_path(conn, :create), guitar: @create_attrs)

      assert redirected_to(conn) == Routes.guitar_path(conn, :index)
    end

    test "renders errors when params are missing", %{conn: conn} do
      response =
        conn
        |> post(Routes.guitar_path(conn, :create), %{})
        |> json_response(400)

      ^response = %{
        "message" => "Bad request",
        "errors" => %{
          "guitar" => %{
            "is_available" => "is required",
            "model" => "is required",
            "year" => "is required"
          }
        }
      }
    end

    test "renders errors when data is invalid", %{conn: conn} do
      response =
        conn
        |> post(Routes.guitar_path(conn, :create), guitar: @invalid_attrs)
        |> json_response(422)

      assert response ==
               %{
                 "errors" => %{
                   "is_available" => ["can't be blank"],
                   "model" => ["can't be blank"],
                   "year" => ["can't be blank"]
                 },
                 "message" => "Unprocessable entity"
               }
    end
  end
end
