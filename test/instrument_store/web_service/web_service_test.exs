defmodule InstrumentStore.WebServiceTest do
  use InstrumentStore.DataCase, async: true

  alias InstrumentStore.Instruments.Guitar
  alias InstrumentStore.Repo
  alias InstrumentStore.WebService

  describe "web service" do
    test "get_stolen_status/1 makes call to web service and updates guitar in database" do
      attrs = %{
        is_available: true,
        model: "Jackson",
        year: 42
      }

      {:ok, %Guitar{is_available: is_available, model: model, year: year} = guitar} =
        %Guitar{}
        |> Guitar.changeset(attrs)
        |> Repo.insert()

      WebServiceBehaviourMock
      |> expect(:get_stolen_status, fn url ->
        expected_url =
          "https://calm-beach-16451.herokuapp.com/search?model=#{attrs.model}&year=#{attrs.year}"

        assert url == expected_url

        %{"isStolen" => true}
      end)

      assert {:ok,
              %Guitar{is_available: ^is_available, model: ^model, year: ^year, is_stolen: true}} =
               WebService.get_stolen_status(guitar)
    end

    test "get_stolen_status/1 returns error and dont update already created guitar" do
      attrs = %{
        is_available: true,
        model: "Jackson",
        year: 42
      }

      {:ok, guitar} =
        %Guitar{}
        |> Guitar.changeset(attrs)
        |> Repo.insert()

      WebServiceBehaviourMock
      |> expect(:get_stolen_status, fn _url ->
        {:error, "Invalid access token"}
      end)

      assert WebService.get_stolen_status(guitar) == {:error, "Invalid access token"}

      assert Repo.get_by!(Guitar, attrs)
    end
  end
end
