defmodule InstrumentStore.Instruments.GuitarTest do
  use InstrumentStore.DataCase, async: true

  alias InstrumentStore.Instruments.Guitar

  describe "changeset/2" do
    @valid_attrs %{is_available: true, model: "Jackson", year: 42}

    @valid_attrs %{is_available: true, model: "Jackson", year: 42}

    test "return valid changeset" do
      changeset = Guitar.changeset(%Guitar{}, @valid_attrs)

      assert changeset.valid?
      assert changeset.changes == @valid_attrs
    end

    test "validate if required fields are present" do
      changeset = Guitar.changeset(%Guitar{}, %{})

      refute changeset.valid?

      assert errors_on(changeset) == %{
               model: ["can't be blank"],
               year: ["can't be blank"]
             }
    end
  end
end
