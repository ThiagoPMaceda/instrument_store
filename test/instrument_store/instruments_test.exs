defmodule InstrumentStore.InstrumentsTest do
  use InstrumentStore.DataCase, async: true

  alias InstrumentStore.Instruments
  alias InstrumentStore.Instruments.Guitar
  alias InstrumentStore.Repo

  import InstrumentStore.InstrumentsFixtures

  describe "guitars" do
    @invalid_attrs %{is_available: nil, model: nil, year: nil}

    @valid_attrs %{is_available: true, model: "Jackson", year: 42}

    test "list_guitars/0 returns all guitars" do
      guitar = guitar_fixture()
      assert Instruments.list_guitars() == [guitar]
    end

    test "get_guitar!/1 returns the guitar with given id" do
      guitar = guitar_fixture()
      assert Instruments.get_guitar!(guitar.id) == guitar
    end

    test "create_guitar/1 with valid data and enqueue worker" do
      assert {:ok, pid} = Instruments.create_guitar(@valid_attrs)

      assert is_pid(pid)

      assert Repo.get_by!(Guitar, @valid_attrs)
    end

    test "create_guitar/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Instruments.create_guitar(@invalid_attrs)
    end

    test "delete_guitar/1 deletes the guitar" do
      guitar = guitar_fixture()
      assert {:ok, %Guitar{}} = Instruments.delete_guitar(guitar)
      assert_raise Ecto.NoResultsError, fn -> Instruments.get_guitar!(guitar.id) end
    end
  end
end
