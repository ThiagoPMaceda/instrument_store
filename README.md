# InstrumentStore

Este projeto é um simples CRUD de uma loja de instrumentos musicais.
A feature que precisamos implementar é adicionar uma **verificação na hora de criar instrumentos.**

Ao criar um instrumento, precisamos fazer uma chamada a um web service externo para checar se o instrumento é roubado. Caso seja identificado como roubado através da resposta do web service, precisamos indicar o mesmo no index de instrumentos e no show do instrumento em questão.

Abaixo, um exemplo de uma chamada ao web service:

```bash
curl --header "X-ACCESS-TOKEN: cTdYDb6gKOAa6jXoAgJzhYz9BYDpEKsKZdsv+i" \
"https://calm-beach-16451.herokuapp.com/search?model=Jackson&year=2020"
```

Note que enviamos o model e o year como parâmetros para o path /search, e passamos um token de autenticação através do header X-ACCESS-TOKEN.

Note que as vezes a resposta pode demorar a ser recebida. Que chato, né ?

Talvez algumas das propriedades do Elixir possam ajudar com isso... 💡

Instruções:

1. Ao concluir a feature, criar um Merge Request para este repositório.
2. Atualizar o README com instruções para rodar a aplicação no ambiente local.
3. Não é necessário preocupar-se com deployment.
4. A escrita de testes é **altamente** recomendada.
5. Divirta-se!


## Instruções para executar a aplicação

Existem duas formas de executar a aplicação, recomendo utilizar a primeira que usa o Docker-compose para isso. 

1. Docker-compose

Para utilizar o projeto utilizando o Docker-compose é necessário: 

     - Fazer o clone do projeto executando o comando `git clone https://gitlab.com/ThiagoPMaceda/instrument_store.git`.
     - Instalar o [Docker Compose](https://docs.docker.com/compose/gettingstarted/).
     - Rodar o comando `docker-compose build`.
     - Rodar o comando `docker-compose up -d`.
     - Após alguns segundos você podera utilizar na url [`localhost:4000`](http://localhost:4000) do seu navegador.

2. Localmente

Para conseguir utilizar o projeto localmente é necessário seguir o passo a passo abaixo:
 
     - Ter instalado o Erlang na versão 24.0.6, você pode instalar utilizando um version manager como o [asdf](https://asdf-vm.com/guide/getting-started.html) ou você pode seguir as instruções no [site do erlang](https://www.erlang.org/patches/otp-24.0.6).
     - Ter instalado o Elixir na versão 1.12.2, você pode instalar utilizando um version manager como o [asdf](https://asdf-vm.com/guide/getting-started.html) ou  você pode seguir as instruções no [site do elixir](https://elixir-lang.org/install.html).
     - Banco de dados Postgres com usuário `postgres` e senha `postgres`, caso a usuário ou senha sejam diferentes você deverá alterar nos arquivos de `config/dev.exs` e `config/test.exs`.
     - Fazer o clone do projeto executando o comando `git clone https://gitlab.com/ThiagoPMaceda/instrument_store.git`.
     - Alterar o arquivo de configuração `dev.exs` nas configurações de banco de dados de `hostname: System.get_env("DATABASE_HOST", "db"),` para `hostname: System.get_env("DATABASE_HOST", "localhost"),`
     - Instalar as dependências `mix deps.get`
     - Para criar e migrar o banco de dados `mix ecto.setup`
     - Para começar o projeto `mix phoenix.server` 
     - Com isso você pode ir na url [`localhost:4000`](http://localhost:4000) do seu navegador.
