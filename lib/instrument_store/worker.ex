defmodule InstrumentStore.Worker do
  alias Task.Supervisor

  def get_guitar_stolen_status(guitar_changeset) do
    opts = [restart: :transient]

    Supervisor.start_child(
      __MODULE__,
      InstrumentStore.WebService,
      :get_stolen_status,
      [guitar_changeset],
      opts
    )
  end
end
