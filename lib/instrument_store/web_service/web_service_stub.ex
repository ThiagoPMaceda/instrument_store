defmodule InstrumentStore.WebServiceStub do
  alias InstrumentStore.WebServiceBehaviour
  @behaviour WebServiceBehaviour

  @impl WebServiceBehaviour
  def get_stolen_status(_attrs), do: %{"isStolen" => true}
end
