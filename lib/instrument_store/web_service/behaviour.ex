defmodule InstrumentStore.WebServiceBehaviour do
  @callback get_stolen_status(binary()) :: map() | {:error, binary()}
end
