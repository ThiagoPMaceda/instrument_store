defmodule InstrumentStore.WebService do
  @moduledoc """
  The External Web Service context.
  """

  @base_url "https://calm-beach-16451.herokuapp.com/search?"

  alias Ecto.Changeset
  alias HTTPoison.Error
  alias InstrumentStore.Instruments.Guitar
  alias InstrumentStore.Repo
  alias InstrumentStore.WebService.HTTPClient

  def get_stolen_status(guitar_changeset) do
    sanitized_model = remove_white_space(guitar_changeset.model)

    url =
      @base_url <>
        "model=#{sanitized_model}&year=#{guitar_changeset.year}"

    update_guitar_in_database(http_client().get_stolen_status(url), guitar_changeset)
  end

  defp update_guitar_in_database(%{"isStolen" => stolen_status}, attrs) do
    Guitar
    |> Repo.get!(attrs.id)
    |> Changeset.change(is_stolen: stolen_status)
    |> Repo.update()
  end

  defp update_guitar_in_database({:error, %Error{reason: reason}}, _attrs), do: {:error, reason}

  defp update_guitar_in_database({:error, _reason} = error, _attrs), do: error

  defp remove_white_space(string) do
    String.replace(string, " ", "_")
  end

  defp http_client do
    Application.get_env(:instrument_store, :web_service, HTTPClient)
  end
end
