defmodule InstrumentStore.WebService.HTTPClient do
  alias HTTPoison.{Error, Response}

  alias InstrumentStore.WebServiceBehaviour

  @behaviour WebServiceBehaviour

  @fifty_seconds_in_milliseconds 50_000
  @web_service_token "cTdYDb6gKOAa6jXoAgJzhYz9BYDpEKsKZdsv+i"

  @impl WebServiceBehaviour
  def get_stolen_status(url) do
    headers = ["X-ACCESS-TOKEN": @web_service_token]

    options = [
      timeout: @fifty_seconds_in_milliseconds,
      recv_timeout: @fifty_seconds_in_milliseconds
    ]

    url
    |> HTTPoison.get(headers, options)
    |> handle_response_from_web_service()
  end

  defp handle_response_from_web_service({:ok, %Response{status_code: 200, body: body}}) do
    Jason.decode!(body)
  end

  defp handle_response_from_web_service({:error, %Error{reason: reason}}), do: {:error, reason}
end
