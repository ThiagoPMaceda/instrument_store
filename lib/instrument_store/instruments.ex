defmodule InstrumentStore.Instruments do
  @moduledoc """
  The Instruments context.
  """

  import Ecto.Query, warn: false

  alias InstrumentStore.Instruments.Guitar
  alias InstrumentStore.Repo
  alias InstrumentStore.Worker

  def list_guitars do
    Repo.all(Guitar)
  end

  def get_guitar!(id), do: Repo.get!(Guitar, id)

  def create_guitar(%{is_available: _is_available, model: _model, year: _year} = params) do
    %Guitar{}
    |> Guitar.changeset(params)
    |> Repo.insert()
    |> enqueue_worker()
  end

  def delete_guitar(%Guitar{} = guitar) do
    Repo.delete(guitar)
  end

  def change_guitar(%Guitar{} = guitar, attrs \\ %{}) do
    Guitar.changeset(guitar, attrs)
  end

  defp enqueue_worker({:error, _reason} = error), do: error

  defp enqueue_worker({:ok, changeset}), do: Worker.get_guitar_stolen_status(changeset)
end
