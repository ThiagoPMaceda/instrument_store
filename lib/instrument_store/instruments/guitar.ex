defmodule InstrumentStore.Instruments.Guitar do
  use Ecto.Schema
  import Ecto.Changeset

  @required_params [:model, :year, :is_available]
  @optional_params [:is_stolen]

  schema "guitars" do
    field :is_available, :boolean, default: false
    field :model, :string
    field :year, :integer
    field :is_stolen, :boolean

    timestamps()
  end

  @doc false
  def changeset(guitar, attrs) do
    guitar
    |> cast(attrs, @required_params ++ @optional_params)
    |> validate_required(@required_params)
  end
end
