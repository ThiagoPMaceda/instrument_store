defmodule InstrumentStoreWeb.ErrorView do
  use InstrumentStoreWeb, :view

  def render("400.json", %{keys: keys}) do
    %{
      "message" => "Bad request",
      "errors" => keys
    }
  end

  def render("422.json", %{errors: errors}) do
    %{
      "message" => "Unprocessable entity",
      "errors" => errors
    }
  end

  def template_not_found(template, _assigns) do
    Phoenix.Controller.status_message_from_template(template)
  end
end
