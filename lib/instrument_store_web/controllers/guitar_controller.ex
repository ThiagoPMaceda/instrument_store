defmodule InstrumentStoreWeb.GuitarController do
  use InstrumentStoreWeb, :controller

  alias InstrumentStore.Instruments
  alias InstrumentStore.Instruments.Guitar

  action_fallback(InstrumentStoreWeb.Fallback)

  filter_for(:create,
    required: [guitar: [:is_available, :model, :year]]
  )

  def index(conn, _params) do
    guitars = Instruments.list_guitars()
    render(conn, "index.html", guitars: guitars)
  end

  def new(conn, _params) do
    changeset = Instruments.change_guitar(%Guitar{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{guitar: guitar_params}) do
    guitar_params
    |> Instruments.create_guitar()
    |> render_response(conn)
  end

  def show(conn, %{"id" => id}) do
    guitar = Instruments.get_guitar!(id)
    render(conn, "show.html", guitar: guitar)
  end

  def delete(conn, %{"id" => id}) do
    guitar = Instruments.get_guitar!(id)
    {:ok, _guitar} = Instruments.delete_guitar(guitar)

    conn
    |> put_flash(:info, "Guitar deleted successfully.")
    |> redirect(to: Routes.guitar_path(conn, :index))
  end

  defp render_response({:error, _changeset} = error, _conn), do: error

  defp render_response({:ok, _guitar}, conn) do
    conn
    |> put_flash(:info, "Guitar created successfully.")
    |> redirect(to: Routes.guitar_path(conn, :index))
  end
end
