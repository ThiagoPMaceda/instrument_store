defmodule InstrumentStoreWeb.Fallback do
  use Phoenix.Controller

  alias Ecto.Changeset
  alias InstrumentStoreWeb.ErrorView
  alias StrongParams.Error

  def call(conn, {:error, :bad_request}) do
    conn
    |> put_status(:bad_request)
    |> put_view(ErrorView)
    |> render("400.json")
  end

  def call(conn, %Error{errors: errors}) do
    conn
    |> put_status(:bad_request)
    |> put_view(ErrorView)
    |> render("400.json", %{keys: errors})
  end

  def call(conn, {:error, error}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(ErrorView)
    |> render("422.json", errors: format_errors(error))
  end

  defp format_errors(error) do
    Changeset.traverse_errors(error, fn {message, opts} ->
      Regex.replace(~r"%{(\w+)}", message, fn _, key ->
        opts |> Keyword.get(String.to_existing_atom(key), key) |> to_string()
      end)
    end)
  end
end
