defmodule InstrumentStore.Repo.Migrations.AddStolenFieldToGuitarsSchema do
  use Ecto.Migration

  def change do
    alter table("guitars") do
      add :is_stolen, :boolean
    end
  end
end
